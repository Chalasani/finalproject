<%@ include file="/WEB-INF/views/includes/include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!doctype html>
<html lang="en">
<head>
<title>Parking</title>
<!-- Bootstrap 4.5 CSS-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<!-- Bootstrap JS Requirements -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
	integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<br>
		<br>
		<br>
		<br>
		
		<div class="row">
			<div class="col-12">
				<center> <h1 class="" >New Permit</h1></center>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12">
				<form:form method="post">
					<div class="form-group">
						<label for="">Firstname</label>
						<input type="text" form:input class="form-control" name="firstName" id="firstName" aria-describedby="" >
					</div>
					<div class="form-group">
						<label for="">Lastname</label>
						<input type="text" form:input class="form-control" id="lastName" name="lastName" aria-describedby="">
					</div>
					<div class="form-group">
						<label for="">make</label>
						<input type="text" form:input class="form-control" id="make" aria-describedby="" name="make">
					</div>
					<div class="form-group">
						<label for="">model</label>
						<input type="text" form:input class="form-control" id="model" aria-describedby="" name="model">
					</div>
					<div class="form-group">
						<label for="">vehicle year</label>
						<input type="text" form:input class="form-control" id="vehicleYear" aria-describedby="" name="vehicleYear" >
					</div>
					<br>
					<center><button type="submit" class="btn btn-primary">Submit</button></center>
				</form:form>
			</div>
		</div>
	</div>

</body>
</html>
