<%@ include file="/WEB-INF/views/includes/include.jsp" %>
<!doctype html>
<html lang="en">
<head>
	<title>MSU Transportation Office</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<br><br><br><br><center><h1>Welcome to the MSU Transportation Office</h1></center>
			</div>
		</div>
		<br><br><br><br><br><br>
		<div class="row">
			<div class="col-12 col-sm-12">
				<table class="table table-hover table-striped table-bordered">
					<tr>
						<th>ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Make</th>
						<th>Model</th>
						<th>Year</th>
					</tr>
					<!-- 
						* Manually Insert Data into your table and then
						* Build a table here and loop through the data and display it	
					-->
					<!-- Handle when on data found -->
					<c:if test="${empty parkingList}">
						<tr><td colspan="6">No Records Found</td></tr>
					</c:if>
					<!-- Loop Here -->
					<c:if test="${not empty parkingList}">
						<!-- 
							 Add <tr> elements and <td> elements below inside 
							 of the c:forEach tags (build the body of the table)
						-->
						<c:forEach var="parkingPass" items="${parkingList}" > 
							
							<tr>
								<td colspan="1">${parkingPass.id}</td>
								<td colspan="1">${parkingPass.firstName}</td>
								<td colspan="1">${parkingPass.lastName}</td>
								<td colspan="1">${parkingPass.make}</td>
								<td colspan="1">${parkingPass.model}</td>
								<td colspan="1">${parkingPass.vehicleYear}</td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
				<br>
				<center>
					<a href="<%=request.getContextPath()%>/newPermit" class="btn btn-primary" >new permit</a>
				</center>
			
			</div>
			
		</div>
	</div>
</body>
</html>
