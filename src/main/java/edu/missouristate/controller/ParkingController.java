package edu.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import edu.missouristate.domain.ParkingPermit;
import edu.missouristate.service.ParkingService;

@Controller
public class ParkingController {

	@Autowired
	ParkingService parkingService;
	
	@GetMapping(value="/")
	public String getIndex(Model model) {
		List<ParkingPermit> parkingList = parkingService.getParkingPasses();
		model.addAttribute("parkingList", parkingList);
		return "index";
	}

	@GetMapping(value = "/newPermit")
	public String getAddAddress(Model x) {

		return "newPermit";
	}

	@PostMapping(value = "/newPermit")
	public String postAddAddress(Model model, ParkingPermit parkingPermit) {
		parkingService.saveParking(parkingPermit);
		return "redirect:/";
	}
	
	
}
