package edu.missouristate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.ParkingPermit;
import edu.missouristate.repository.ParkingRepository;

@Service("parkingService")
public class ParkingService {

	@Autowired
	ParkingRepository parkingRepo;

	public List<ParkingPermit> getParkingPasses() {
		return parkingRepo.getParkingPasses();
	}

	public void saveParking(ParkingPermit parkingPermit) {
		parkingRepo.createParkingPass(parkingPermit);
	}
	
}
